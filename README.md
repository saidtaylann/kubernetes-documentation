### _This documentation is a comprehensive guide and learning path for Kubernetes like i learnt._
### _You can access the documentation by click the html file on the this directory._
### _This README file only involve headings on the documentation also it has not its content._

# Kubernetes Documentation

[Introduction to Documentation](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Introduction%20to%20Documentation%20c7e24d5c676a44da8fd61fe510c3835a.md)

[Kubernetes Nedir?](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Kubernetes%20Nedir%20edd06f9ef6d54285905213e251f5fc0d.md)

[Kubernetes Yapısı](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Kubernetes%20Yap%C4%B1s%C4%B1%20231b6cae7336415284e8bef92cf5c628.md)

[Kubernetes & Docker vs containerd](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Kubernetes%20&%20Docker%20vs%20containerd%2095d54a6501e94440a138da9d867722a3.md)

[Temel Konseptler](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Temel%20Konseptler%20a745e118ff4b49e7ae57c74406628bb4.md)

[Scheduling](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Scheduling%20edc474b818bc473fab4788fdd8dc8f53.md)

[Pod Design](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Pod%20Design%20ce38564036514b84bb116fb5c79e85ef.md)

[Configuration](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Configuration%20e8af98c545934e1e9afd81a134c78055.md)

[Logging & Monitoring](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Logging%20&%20Monitoring%20a5854d68a33640809c15dcffb09cc3ee.md)

[Networking](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Networking%2069aa6cdc5076442d83b7a20beda2a0d4.md)

[Storage](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Storage%20d3251c41d97f4ecb92880274c9b36b5e.md)

[Security](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Security%20d062a6f71a764b7685b5a9c19e14883f.md)

[Cluster Maintenance](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Cluster%20Maintenance%209aa31bb247824379b2fe6a0a4fcbf0c0.md)

[Design and Install a Kubernetes Cluster](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Design%20and%20Install%20a%20Kubernetes%20Cluster%2081c8f75f4c184c90a9c60fc611da8894.md)

[Troubleshooting](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Troubleshooting%20b41722edda244285b6f72abc7d47735a.md)

[Extras](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/Extras%20642971c890c34c59aba6f23a58974394.md)

[TODOs](Kubernetes%20Do%CC%88ku%CC%88mantasyon%2003046c84293348e89fe4aa5f7783637c/TODOs%2077361ff67b474c4a9d0fb92cceb274e5.md)
